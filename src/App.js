import React, { useState } from "react";
import logo from "./logo.svg";

function App() {
  // state for storing the base64 images
  const [img, setImg] = useState([]);

  // function to convert image file into base64
  const handleImage = (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      setImg([...img, reader.result]);
    };
    reader.onerror = () => {
      console.log("error");
    };
  };
  console.log(img);
  // clear state
  const clearImage = () => {
    if (img.length) {
    setImg([]);
    } else {
      alert("gamber kosong!")
    }
  };
  return (
    <div>
      <div>
        <h1>Preview</h1>
        {/* preview latest image added to state */}
        <img src={img.length ? img[img.length - 1] : logo} alt="img" />
        <input type="file" onChange={handleImage} />
        <button onClick={clearImage}>Clear</button>
      </div>
      <div>
        <h1>All Images</h1>
        {img.length
        //mapping image state
          ? img.map((data, index) => {
              return (
                <div key={index}>
                  <img src={data} alt="mapped img" />
                </div>
              );
            })
          : null}
      </div>
    </div>
  );
}

export default App;
